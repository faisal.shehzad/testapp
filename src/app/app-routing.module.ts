import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: '', component: AppComponent},
  { path: 'agent', loadChildren:()=> import('../../projects/agent/src/app/app.module').then(c=>c.AppModule ) },
  { path: 'insights', loadChildren:()=> import('../../projects/agent/src/app/app.module').then(c=>c.AppModule ) },
  { path: '**', component: AppComponent}
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
