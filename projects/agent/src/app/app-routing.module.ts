import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { View1Component } from 'projects/agent/src/app/view1/view1.component';
import { View2Component } from 'projects/agent/src/app/view2/view2.component';

const routes: Routes = [
  { path: '', component: View1Component },
  { path: 'two', component: View2Component },
  { path: 'agent', redirectTo: 'agent/one' }
 ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
